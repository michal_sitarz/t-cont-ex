var hashSpan = document.getElementById('interactionsHash');

window.parent.postMessage('contentLoaded', '*');

function click() {
    setTimeout(function() {
        window.document.getElementsByTagName('canvas')[0].dispatchEvent(new Event('mousedown'));
    }, 0);
    
    setTimeout(function() {
        window.document.getElementsByTagName('canvas')[0].dispatchEvent(new Event('mouseup'));            
    }, 50); 
}    

window.totem = {
    restart: function() { window.gamee.onRestart(); },
    start: click,
    click: click,
    score: function() {
        return window.gamee.score;
    }
};


window.addEventListener("message", receiveMessage, false);

function receiveMessage(event)
{
    console.log('event',event);


    if (typeof event.data !== 'object') {
        console.error('event data is not an object');
        return;
    }

    if (event.data.event === 'displayHash') {
        hashSpan.innerText = event.data.data;
    }
    else {

//        if (event.data.event == 'click' && )
    
        var origin = event.origin || event.originalEvent.origin; // For Chrome, the origin property is in the event.originalEvent object.

        window.totem[event.data.event]();
    }


}

//window.parent.totem = window.totem;
