'use strict';
var socket = io.connect('http://' + 'totem-core-interactions.stage.toteminteractive.io'); 
   
var tap_button = document.querySelector('.tap');
var restart_button = document.querySelector('.restart');
var connect_button = document.querySelector('.connect');
var error = document.querySelector('.error');
error.innerText = 'disconnected';

var connect = function(hash) {    
    if(hash){
        init(socket, hash);
    } else {
        error.innerText = 'No hash provided. You should specify url param: "www.url.com?hash=12345"'; 
    }
}

var init = function(socket, hash){
    var emit = function(event, data){
        if(socket){
           if(event === 'message'){                    
               if(data.data){
                   socket.emit(event, JSON.stringify(data));
               } else {
                   console.log('Incorrect message. It has to contain "data" property with object as a value, like {data : {}}', {event:event, data:data});
               }
           } else {
               socket.emit(event, data);
           }
            console.log('emitted: ' + event, data); 
        } else {
            console.log('no socket available');
        }
    };
    
    var deviceConnected = Rx.Observable.fromEvent(socket, 'device-connected'); 
    var connectionFailed = Rx.Observable.fromEvent(socket, 'device-connect-error');
    var connectionRejected = Rx.Observable.fromEvent(socket, 'session-rejected'); 
    
    connectionFailed
        .take(1)
        .subscribe(
            function(x)  {
                error.innerText = x.err; 
                socket.disconnect(); 
            });
            
    connectionRejected
        .take(1)
        .subscribe(
            function()  {
                error.innerText = 'Connections limit error'; 
                socket.disconnect(); 
            });
            
    var movement = new Rx.Subject();
        
    var moves = movement
        .sample(100)
        .filter(function(x) {
            return x.rotationRate.alpha > 1; 
        });
        
    var clicks = Rx.Observable.fromEvent(tap_button,"click");

    // window.ondevicemotion = function(event){
    //     movement.onNext(event);
    // };
    
    deviceConnected
        .take(1)
        .do(function(x) { console.log('device-connected', x);})
        .concatMap(function(x)  {
            error.innerText = 'Connected';  
            return Rx.Observable.merge(moves, clicks)
                .takeUntil(connections);
        })
        .subscribe(
            function()  {                
                emit('message', {event:'click', data:[]});
            },
            function(e) {error.innerText = 'device connected error';});
            
    restarts
        .subscribe(
            function()  {
                emit('message', {event:'restart', data:[]});
            }, 
            function(e) {error.innerText = 'restart error';});
    
    emit('device-connect', {deviceId : getId(), hash : hash});
}
 
var connections = Rx.Observable
    .fromEvent(connect_button, 'click')
        .map(function(x) {return document.getElementById("hash").value;})
    .singleInstance();
    
var restarts = Rx.Observable
    .fromEvent(restart_button, 'click')
    .singleInstance();
    
connections
    .subscribe(
        function(hash)  {
            console.log('connect', hash);
            connect(hash);
        },
        function(e) {error.innerText = 'connections error';});
    
function getId() {
    var id = localStorage.getItem('deviceId');
    if(!id){
        id = makeid();
        localStorage.setItem('deviceId', id);    
    }
    
    return id;
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}



 
